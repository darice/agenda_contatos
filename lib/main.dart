import 'package:flutter/material.dart';
import 'package:agenda_contatos/pages/home_page.dart';

void main() {
  runApp(MaterialApp(
    home: HomePage(),
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
        backgroundColor: Colors.green,
        primaryColor: colorMain,
        cursorColor: colorMain,
        hintColor: colorMain,
        appBarTheme: AppBarTheme(
          color: colorMain,
        )),
  ));
}

Color colorMain = Colors.red[400];
Color colorBackground = Colors.red[100];
