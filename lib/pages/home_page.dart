import 'dart:io';

import 'package:agenda_contatos/helpers/contatos_helper.dart';
import 'package:flutter/material.dart';
import 'package:agenda_contatos/main.dart';
import 'package:agenda_contatos/pages/contato_page.dart';
import 'package:url_launcher/url_launcher.dart';

enum Ordenar { ordenarAZ, ordenarZA } //enumerador: é um conjunto de constantes

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ContatosHelper helper = ContatosHelper();
  List<Contato> contatosLista = List();

  @override
  void initState() {
    super.initState();

    // Contato c = Contato();
    // c.nome = 'Daise';
    // c.email = 'daisesousa100@gmail.com';
    // c.telefone = '988086868';
    // c.foto = 'imgtest';
    // helper.salvarContato(c);

    // helper.pegarTodosContatos().then((lista) => print(lista));
    atualizarContatosLista();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Contatos"),
        centerTitle: true,
        actions: <Widget>[
          PopupMenuButton<Ordenar>(
            itemBuilder: (context) => <PopupMenuEntry<Ordenar>>[
              const PopupMenuItem<Ordenar>(
                child: Text("Ordenar de A-Z"),
                value: Ordenar.ordenarAZ,
              ),
              const PopupMenuItem(
                child: Text("Ordenar de Z-A"),
                value: Ordenar.ordenarZA,
              )
            ],
            onSelected: ordenarLista,
          ),
        ],
      ),
      backgroundColor: colorBackground,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          chamarContatoPage();
        },
        child: Icon(Icons.add),
        backgroundColor: colorMain,
      ),
      body: ListView.builder(
        padding: EdgeInsets.all(10),
        itemCount: contatosLista.length,
        itemBuilder: (context, index) {
          return contatoCartao(context, index);
        },
      ),
    );
  }

  Widget contatoCartao(BuildContext context, int index) {
    return SingleChildScrollView(
      child: GestureDetector(
        child: Card(
          child: Padding(
            padding: EdgeInsets.all(10),
            child: Row(
              children: <Widget>[
                Container(
                  width: 60,
                  height: 60,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: contatosLista[index].foto != null
                          ? FileImage(File(contatosLista[index].foto))
                          : AssetImage("images/pessoa.png"),
                      // image: AssetImage("images/pessoa.png"),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    left: 10,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        contatosLista[index].nome ?? '',
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                      Text(contatosLista[index].email ?? ''),
                      Text(contatosLista[index].telefone ?? ''),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        onTap: () {
          mostrarOpcoes(context, index);
        },
      ),
    );
  }

  void chamarContatoPage({Contato contatoQueVouEditar}) async {
    final contatoQueRecebi = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) {
          return ContatoPage(contato: contatoQueVouEditar);
        },
      ),
    );
    if (contatoQueRecebi != null) {
      if (contatoQueVouEditar != null) {
        await helper.atualizarContato(contatoQueRecebi);
      } else {
        await helper.salvarContato(contatoQueRecebi);
      }
      atualizarContatosLista();
    }
  }

  void atualizarContatosLista() {
    helper.pegarTodosContatos().then((lista) {
      setState(() {
        contatosLista = lista;
      });
    });
  }

  void mostrarOpcoes(BuildContext context, int index) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return BottomSheet(
            onClosing: () {},
            builder: (context) {
              return Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    botaoMostrarOpcoes("Ligar", () {
                      launch("tel: ${contatosLista[index].telefone}");
                      Navigator.pop(context);
                    }),
                    botaoMostrarOpcoes("Editar", () {
                      Navigator.pop(context);
                      chamarContatoPage(
                          contatoQueVouEditar: contatosLista[index]);
                    }),
                    botaoMostrarOpcoes("Excluir", () {
                      Navigator.pop(context);
                      helper.deletarContato(contatosLista[index].id);
                      setState(() {
                        contatosLista.removeAt(index);
                      });
                    })
                  ],
                ),
              );
            },
          );
        });
  }

  Widget botaoMostrarOpcoes(String texto, Function onPressed) {
    return FlatButton(
        onPressed: onPressed,
        child: Text(
          texto,
          style: TextStyle(color: colorMain),
        ));
  }

  void ordenarLista(Ordenar opcao) {
    switch (opcao) {
      case Ordenar.ordenarAZ:
        print("ok");
        setState(() {
          contatosLista.sort((a, b) {
            return a.nome.toLowerCase().compareTo(b.nome.toLowerCase());
          });
        });
        break;
      case Ordenar.ordenarZA:
        print("okay");
        setState(() {
          contatosLista.sort((a, b) {
            return b.nome.toLowerCase().compareTo(a.nome.toLowerCase());
          });
        });
        break;
    }
  }
} //fim
