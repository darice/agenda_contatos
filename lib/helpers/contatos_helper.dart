import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';

String contatoTabela = "contatoTabela";
final String idColuna = "idColuna";
final String nomeColuna = 'nomeColuna';
final String emailColuna = 'emailColuna';
final String telefoneColuna = 'telefoneColuna';
final String fotoColuna = 'fotoColuna';

class ContatosHelper {
  static final ContatosHelper _instancia = ContatosHelper.interno();

  factory ContatosHelper() {
    return _instancia;
  }

  ContatosHelper.interno();

  Database _bd;

  Future<Database> get bd async {
    if (_bd != null) {
      return _bd;
    } else {
      _bd = await iniciarBD();
      return _bd;
    }
  }

  Future<Database> iniciarBD() async {
    final bdLocal = await getDatabasesPath();
    final bdArquivo = join(bdLocal, "contatosnew.bd");

    return await openDatabase(
      bdArquivo,
      version: 1,
      onCreate: (Database bd, int versaoRecente) async {
        await bd.execute(
            "CREATE TABLE $contatoTabela($idColuna INTEGER PRIMARY KEY, $nomeColuna TEXT, $emailColuna TEXT, $telefoneColuna TEXT, $fotoColuna TEXT)");
      },
    );
  }

  Future<Contato> salvarContato(Contato newContato) async {
    Database bdContato = await bd;
    newContato.id =
        await bdContato.insert(contatoTabela, newContato.deContatoParaMapa());
    return newContato;
  }

  Future<Contato> pegarContato(int id) async {
    Database bdContato = await bd;
    List<Map> mapas = await bdContato.query(
      contatoTabela,
      columns: [
        idColuna,
        nomeColuna,
        emailColuna,
        telefoneColuna,
        fotoColuna,
      ],
      where: "$idColuna = ?",
      whereArgs: [id],
    );
    if (mapas.length > 0) {
      return Contato.deMapaParaContato(mapas.first);
    } else {
      return null;
    }
  }

  Future<int> deletarContato(int id) async {
    Database bdContato = await bd;
    return await bdContato.delete(
      contatoTabela,
      where: "$idColuna = ?",
      whereArgs: [id],
    );
  }

  Future<int> atualizarContato(Contato contato) async {
    Database bdContato = await bd;
    return bdContato.update(
      contatoTabela,
      contato.deContatoParaMapa(),
      where: "$idColuna = ?",
      whereArgs: [contato.id],
    );
  }

  Future<List> pegarTodosContatos() async {
    Database bdContato = await bd;
    List listaMapa = await bdContato.rawQuery(
      "SELECT * FROM $contatoTabela",
    );
    List<Contato> listaContato = List();
    for (Map m in listaMapa) {
      listaContato.add(Contato.deMapaParaContato(m));
    }
    return listaContato;
  }

  Future<int> pegarNumero() async {
    Database bdContato = await bd;
    return Sqflite.firstIntValue(
      await bdContato.rawQuery(
        "SELECT COUNT (*) FROM $contatoTabela",
      ),
    );
  }

  Future close() async {
    Database bdContato = await bd;
    bdContato.close();
  }
} //fecha ContatosHelper

class Contato {
  int id;
  String nome;
  String email;
  String telefone;
  String foto;

  Contato();

  Contato.deMapaParaContato(Map map) {
    id = map[idColuna];
    nome = map[nomeColuna];
    email = map[emailColuna];
    telefone = map[telefoneColuna];
    foto = map[fotoColuna];
  }

  Map deContatoParaMapa() {
    Map<String, dynamic> mapa = {
      nomeColuna: nome,
      emailColuna: email,
      telefoneColuna: telefone,
      fotoColuna: foto,
    };
    if (id != null) {
      mapa[idColuna] = id;
    }
    return mapa;
  }

  @override
  String toString() {
    return "Contato(id: $id, nome: $nome, email: $email, telefone: $telefone, foto: $foto)";
  }
} //fecha class Contato
